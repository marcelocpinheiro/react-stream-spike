
export default function PixForm({handleSuccess, handleError, payload}) {
    return (
        <div>
            <p>Pix Form</p>
            <button onClick={() => handleSuccess({ data: "success" })}>Sucesso</button>
            <button onClick={() => handleError('pixForm.error')}>Erro</button>
        </div>
    )
}