
export default function CreditCardForm({handleSuccess, handleError, payload}) {
    return (
        <div>
            <p>CreditCardForm</p>
            <button onClick={() => handleSuccess({ data: "success" })}>Sucesso</button>
            <button onClick={() => handleError('creditCardForm.error')}>Erro</button>
        </div>
    )
}