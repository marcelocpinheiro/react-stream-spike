import { useEffect, useState } from "react";

export default function Ghost({handleSuccess, handleError, payload}) {

    const [Component, setComponent] = useState(null);

    useEffect(() => {
        if(!payload.component) {
            handleError('ghost.error')
            setComponent(() => () => {
                return (<div>Falha ao carregar</div>)
            })
        } else {
            setComponent(() => payload.component)
        }
    }, [])

    return (
        <>
        {Component && (<Component handleSuccess={handleSuccess} handleError={handleError} payload={payload} />)}
        </>
    )

}