import { useEffect, useState } from "react"
import CreditCardForm from "../credit-card-form"
import PixForm from "../pix-form"

export default function SelectMethod({ handleSuccess, handleError, payload }) {

    const [selected, setSelected] = useState('credit-card')
    const [selectedComponent, setSelectedComponent] = useState(null)

    useEffect(() => {
        setSelectedComponent(() => {
            return CreditCardForm
        })
    }, [])

    const handleOptionChange = (ev) => {
        const v = ev.target.value
        setSelected(v)
        if(v == 'credit-card') setSelectedComponent(() => {
            return CreditCardForm
        })
        if(v == 'pix') setSelectedComponent(() => {
            return PixForm })
    }

    return (
        <div>
            <select value={selected} onChange={handleOptionChange}>
                <option value={'credit-card'}>Cartao de credito</option>
                <option value={'pix'}>PIX</option>
            </select>
            <button onClick={() => handleSuccess({ component: selectedComponent })}>Sucesso</button>
            <button onClick={() => handleError('selectedMethod.error')}>Erro</button>
        </div>
        

    )
}