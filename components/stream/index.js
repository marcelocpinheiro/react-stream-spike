import { useEffect, useState } from "react";
import { useIsMount } from "../../hooks";

export default function Stream({ stream, handleChange, handleError, handleEnd }) {

    const [Component, setComponent] = useState(null)
    const [streamIndex, setStreamIndex] = useState(0) 
    const [payload, setPayload] = useState({})
    const isMount = useIsMount()

    const changeComponent = () => {
        setComponent(() => stream[streamIndex])
        handleChange(streamIndex, payload)
    }

    useEffect(() => {
        if(stream.length > 0) {
            changeComponent()
        } else {
            setComponent(() => {
                return (<div>Stream cant be empty</div>)
            })
        }
    }, [])

    useEffect(() => {
        if(!isMount) {
            if(streamIndex > stream.length - 1) {
                handleEnd(payload)
            } else {
                changeComponent()
            }
        }   
    }, [streamIndex])

    useEffect(() => {
        if(!isMount) {
            setStreamIndex(streamIndex + 1)
        }
    }, [payload])

    const handleComponentSuccess = (payload) => {
        setPayload(payload)
    }

    return (
        <>
        { Component && (<Component handleSuccess={handleComponentSuccess} handleError={handleError} payload={payload} />) }
        </>
    )

    
}