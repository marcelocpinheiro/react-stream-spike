import Ghost from "../../components/ghost"
import SelectMethod from "../../components/select-method"
import Stream from "../../components/stream"
import Summary from "../../components/summary"

export default function CheckoutTemplateV1() {

    const stream = [
        Summary,
        SelectMethod,
        Ghost
    ]

    const handleChange = (index, payload) => {
        console.log('mudando componente')
    }

    const handleError = (error) => {
        alert(error)
    }

    const handleEnd = (payload) => {
        alert('Fim do stream')
    }

    return (
        <div>
            <h1>Welcome to Template 1</h1>
            <Stream stream={stream} handleChange={handleChange} handleError={handleError} handleEnd={handleEnd} />
        </div>
    )



}