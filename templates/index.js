import CheckoutTemplateV1 from "./checkoutTemplateV1";
import CheckoutTemplateV2 from "./checkoutTemplateV2";
import { useRouter } from 'next/router'

const templates = {
    'checkout-template-v1': CheckoutTemplateV1,
    'checkout-template-v2': CheckoutTemplateV2
}

export default function useTemplate() {
    const defaultTemplate = CheckoutTemplateV1
    const router = useRouter()
    
    const template = router.query.template;
    if(template) {
        return templates[template] || defaultTemplate
    }

    return defaultTemplate;
}