import CreditCardForm from "../../components/credit-card-form"
import Stream from "../../components/stream"
import Summary from "../../components/summary"

export default function CheckoutTemplateV2() {

    const stream = [
        Summary,
        CreditCardForm
    ]

    const handleChange = (index, payload) => {
        console.log('mudando componente')
    }

    const handleError = (error) => {
        alert(error)
    }

    const handleEnd = (payload) => {
        alert('Fim do stream')
    }

    return (
        <div>
            <h1>Welcome to Template 2</h1>
            <Stream stream={stream} handleChange={handleChange} handleError={handleError} handleEnd={handleEnd} />
        </div>
    )



}